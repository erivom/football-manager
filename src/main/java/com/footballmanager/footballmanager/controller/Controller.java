package com.footballmanager.footballmanager.controller;

import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.footballmanager.footballmanager.services.TeamService;

@org.springframework.stereotype.Controller
public class Controller {
    public final TeamService ts;

    public Controller(TeamService ts) {
        this.ts = ts;
    }


    @GetMapping("/")
    public String index (Model model) {
        model.addAttribute("teams", ts.getFootballTeam());
        model.addAttribute("players", ts.getPlayers());
        return "index";
    }

    @PostMapping("/createteam")
    public String createTeam(@RequestParam String name, @RequestParam String country) {
        ts.createTeam(name, country);
        return "redirect:/";
    }

    @PostMapping("/createplayer")
    public String createPlayer(@RequestParam String namePL, @RequestParam int jerseyNumbr, @RequestParam int lifetimeGoals) {
        ts.createPlayer(namePL, jerseyNumbr, lifetimeGoals);
        return "redirect:/";
    }
    @PostMapping("/deleteplayer")
    public String deletePlayer(@RequestParam int id) {
        ts.deletePlayer(id);
        return "redirect:/";
    }
    @PostMapping("/updateplayer")
    public String updatePlayer(@RequestParam int id, @RequestParam int jerseyNumbr, @RequestParam int lifetimeGoals) {
        ts.updatePlayer(id, jerseyNumbr,lifetimeGoals);
        return "redirect:/";
    }
}
