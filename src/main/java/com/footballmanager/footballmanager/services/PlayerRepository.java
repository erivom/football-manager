package com.footballmanager.footballmanager.services;

import com.footballmanager.footballmanager.model.FootballTeam;
import com.footballmanager.footballmanager.model.Player;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface PlayerRepository extends CrudRepository<Player, Integer> {
    List<Player> findAll();

    @Modifying
    @Transactional
    @Query("UPDATE Player p SET  p.jerseyNumbr = :jerseyNumbr, p.lifetimeGoals = :lifetimeGoals WHERE p.id = :id")
    void updatePlayer(@Param("id") int id, @Param("jerseyNumbr") int jerseyNumbr, @Param("lifetimeGoals") int lifetimeGoals);
}
