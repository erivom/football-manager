package com.footballmanager.footballmanager.services;

import com.footballmanager.footballmanager.model.FootballTeam;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FootballTeamRepository extends CrudRepository<FootballTeam,Integer> {
    List<FootballTeam> findAll();
}
