package com.footballmanager.footballmanager.services;

import com.footballmanager.footballmanager.model.FootballTeam;
import com.footballmanager.footballmanager.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.List;
import java.util.Optional;

@Service
public class TeamService {
    @Autowired
    FootballTeamRepository repo;

    @Autowired
    PlayerRepository repoPl;
    public void createTeam(String name, String country) {
        FootballTeam team = new FootballTeam(name, country);
        repo.save(team);
    }

    public List<FootballTeam> getFootballTeam() {
        return repo.findAll();

    }

    public void createPlayer(String namePL, int jerseyNumbr, int lifetimeGoals ) {
        Player player = new Player(namePL,jerseyNumbr, lifetimeGoals);
        repoPl.save(player);
    }

    public List<Player> getPlayers() {
        return repoPl.findAll();

    }

public void deletePlayer(int id) {
        repoPl.deleteById(id);
    }

    @Modifying
    @Query
    public void updatePlayer(int id, int jerseyNumbr, int lifetimeGoals) {
        repoPl.updatePlayer(id,jerseyNumbr, lifetimeGoals);
    }
}
