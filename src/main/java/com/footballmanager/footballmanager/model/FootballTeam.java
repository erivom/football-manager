package com.footballmanager.footballmanager.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class FootballTeam {
    private String name;
    private String country;


    @Id
    @GeneratedValue
    private int id;

    public FootballTeam(String name, String country) {
        this.name = name;
        this.country = country;
        this.id = id;
    }

    public FootballTeam() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
