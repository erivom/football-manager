package com.footballmanager.footballmanager.model;

import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;

@Entity
@DynamicInsert

public class Player {

    private String namePL;
    private int jerseyNumbr;
    private int lifetimeGoals;
    private int id;

    public Player() {
    }

    public Player(String namePL, int jerseyNumbr, int lifetimeGoals) {
        this.namePL = namePL;
        this.jerseyNumbr = jerseyNumbr;
        this.lifetimeGoals = lifetimeGoals;
        this.id = id;
    }


    public String getNamePL() {
        return namePL;
    }

    public void setNamePL(String namePL) {
        this.namePL = namePL;
    }

    public int getJerseyNumbr() {
        return jerseyNumbr;
    }

    public void setJerseyNumbr(int jerseyNumbr) {
        this.jerseyNumbr = jerseyNumbr;
    }

    public int getLifetimeGoals() {
        return lifetimeGoals;
    }

    public void setLifetimeGoals(int lifetimeGoals) {
        this.lifetimeGoals = lifetimeGoals;
    }

    public void setIdPL(int id) {
        this.id = id;
    }


    public int getIdPL() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    public int getId() {
        return id;
    }
}
